<?php
	if(isset($_POST["btnSubmit"])){
		$first_name=trim($_POST["first_name"]);
		$last_name=trim($_POST["last_name"]);
		$father_name=trim($_POST["father_name"]);
		$mother_name=trim($_POST["mother_name"]);
		$gender=trim($_POST["gender"]);		
		$blood_group=trim($_POST["blood_group"]);
		$mobile=trim($_POST["mobile"]);
		$phone=trim($_POST["phone"]);
		$email=trim($_POST["email"]);
		$present_address=trim($_POST["present_address"]);
		$permanent_address=trim($_POST["permanent_address"]);		
		$remarks=trim($_POST["remarks"]);
		
		$yearstart=trim($_POST["year"]);
		$monthstart=trim($_POST["month"]);
		$daystart=trim($_POST["day"]);
		
		$photo_tmp=$_FILES["photo"]["tmp_name"];
		$photo=$_FILES["photo"]["name"];
		$fType=$_FILES["photo"]["type"];
		$size=$_FILES["photo"]["size"];
				
		$movepicture='false';		
		$error=array();
		if($fType=="image/png" || $fType=="image/jpg" || $fType=="image/jpeg" || $fType=="image/gif"){
			if($size<=400000){
				$movepicture='true';
			}else{
						array_push($error,"picture must be within 400KB");
				}				
		}else {
					array_push($error,"picture must be jpg/png/gif format");
			}				
		if(!preg_match("/^[a-zA-Z. ]{2,80}[0-9]*$/",$first_name)){
					array_push($error,"first name must be at least 2 characters");	
			}
		if(!preg_match("/^[a-zA-Z. ]{2,80}[0-9]*$/",$last_name)){
					array_push($error,"last name must be at least 2 characters");	
			}
		if(!preg_match("/^[a-zA-Z. ]{2,80}[0-9]*$/",$father_name)){
					array_push($error,"father's name must be at least 2 characters");	
			}			
		if(!preg_match("/^[a-zA-Z. ]{2,80}[0-9]*$/",$mother_name)){
					array_push($error,"mother's name must be at least 2 characters");	
			}		
			$birth="$monthstart,$daystart,$yearstart";
		if(!preg_match("/^[0-9]+[,][0-9]+[,][0-9]+$/",$birth)){
			array_push($error,"please validate birthdate");			
		}else{
			$result=checkdate($monthstart,$daystart,$yearstart);	
			if(!$result){
			array_push($error,"incorrect birthdate");
			}else{
				$dob="$yearstart:$monthstart:$daystart";
				}
		}	
		if(!preg_match("/^[+]?[-0-9]{8,20}$/",$mobile)){
					array_push($error,"please enter valid mobile number");	
			}	
		if($email==!0){
				if(!preg_match("/^[a-zA-Z]{3,50}[._]?[0-9]*[@][a-zA-Z]{2,15}[.][a-zA-Z0-9]{2,15}$/",$email)){
					array_push($error,"please enter a valid email, special characters only ._ are allowed");	
				}
			}	
		if(count($error)!=0){
				foreach($error as $er){
				echo "<div style='font-weight:bold; color:red'>$er</div>";
				} 
		}else{
			
				if($movepicture!=""){
					 move_uploaded_file($photo_tmp,"adminHome/contents/personal_info/profile_picture/".$photo);
					}
				$db=new mysqli("localhost","root","","supershop") or die("database not found");
				$insert=$db->query("insert into personal_info(first_name,last_name,father_name,mother_name,gender,dob,blood_group,mobile,phone,email,present_address,permanent_address,photo,remarks)values('$first_name','$last_name','$father_name','$mother_name','$gender','$dob','$blood_group','$mobile','$phone','$email','$present_address','$permanent_address','$photo','$remarks')") or die ("could not insert");
	if($insert){
		echo "saved";
		}
	
	}
			}	
?>
