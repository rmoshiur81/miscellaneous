flash:

flash in controller:
Session::flash('message', 'User Successfully Added');

flash in blade:
 @if(Session::has('message'))
   {{ Session::get('message') }}
 @endif
 
 
 edit/delete:
 edit in blade view/index:
  <a href="{{ url('/user/'. $user->id) }}">View</a>|
  <a href="{{ url('/user/'. $user->id. '/edit') }}">Edit</a>
  <a href="{{ url('/user/'. $user->id. '/delete') }}">Delete</a>
  
  route while coming into edit form:
  Route::get('/user/{id}/edit', 'UserController@edit');
  
  edit in blade edit form:
 {!! Form::open(['url' => '/user/'. $user->id. '/update']) !!}
 
 
 edit in controller:
  public function edit($id)
 {
   $user = User::find($id);
   $professoins = Profession::pluck('name', 'id');
   return view('admin.users.edit', ['user' => $user, 'professoins'=> $professoins]);
 }
 
 route while coming into edit form:
 Route::post('/user/{id}/update', 'UserController@update');
 
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $data = $request->only('name', 'profession_id');
        
        $user->update($data);
        
        $profileData = $request->only('bio', 'web', 'facebook', 'twitter', 'github');
        $user->profile()->update($profileData);
        
        Session::flash('message', 'User Successfully Updated');
        return redirect('/users');
    }
        
delete:

 delete in blade view/index:
  <a href="{{ url('/user/'. $user->id. '/delete') }}">Delete</a>
  
  route:
  Route::get('/users', 'UserController@index');
  while click to delete:
  route:
  Route::get('/user/{id}/delete', 'UserController@destroy');
  controller:
        public function destroy($id)
    {
        $user = User::find($id);
        $name = $user->name;
        echo "$name going to destory";
        $user->destroy($id);
        Session::flash('message', "$name Successfully Deleted");
        return redirect('/users');
    }


conditional clause:
$role = $request->input('role');

$users = DB::table('users')
                ->when($role, function ($query) use ($role) {
                    return $query->where('role_id', $role);
                })
                ->get();
                

file upload:
in controller:
if($request->hasFile('image')){
    $request->file('image');
    $request->image->store('public');
    $request->image->extension();
    $request->image->putFile('public', $request->file('image'););
}else}{
return 'no file chosen';
}


ajax method in laravel:

in blade form for post method:
<form id="" action="#">
<meta name="csrf-token" content="{{ csrf-token() }}" />
<input type="hidden" name="_token" value="{{ csrf-token() }}">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $.('meta[name="csrf-token"]').attr('content')
    }
})


verify csrftoken in middleware verifycsrftoken file
protected function tokenMatch($request){
    $token = $request->ajax()? $request->header('X-CSRF-TOKEN'):$request->input('_token');
    return $request->session()->token() == $token;
}

route:
Route::get('/getRequest', function(){
    if(Request::ajax()){
        return('hello for ajax');
    }
    
});
from ajax call:
just call the 'getRequest'


ajax add more:
        function addMoreExperience() {
            var maxField = 3;
            var addButton = $('.addExperience');
            var wrapper = $('.experienceArea');
            var fieldHTML = {
                row: function (f) {
                    return "<div class='form-group exp'>" +
                        "<label class='control-label col-md-3 input-sm'>Experience </label><div class='col-md-9'>" +
                        "<div class='border-js'>" +
                        " <div class='row'><a style='color:red;font-size:20px; float:right' href='javascript:void(0);' class='remove_button' title='Remove field'>&times;</a></div>"
                        +
                        "<div class='row'>" +
                        "<div class='form-group'>" +
                        "<div class='col-md-6'>" +
                        "<input type='text' class='form-control input-sm' " +
                        "name='doctor_employment_histories[" + f + "][designation]' placeholder='Designation'></div>" +
                        "<div class='col-md-6'><input type='text' class='form-control input-sm' name='doctor_employment_histories[" + f + "][organization]' placeholder='organization'></div></div>" +
                        "<div class='form-group'><div class='col-md-6'>" +
                        "<select name='doctor_employment_histories[" + f + "][country_id]' id='country_id' class='form-control'>" +
                        "<option value'>Select Country</option>" +
                        " <?php
                            foreach ($countries as $key => $country) {
                                echo "<option value='$key'>$country</option>";
                            }
                            ?>"
                        + "</select></div><div class='col-md-6'> <input type='text' class='form-control input-sm' name='doctor_employment_histories[" + f + "][city]' placeholder='City'></div></div><div class='form-group'><div class='col-md-6'><input type='text' class='form-control input-sm join_date' name='doctor_employment_histories[" + f + "][join_date]' id='join_date' placeholder='Join Date'> </div><div class='col-md-6'><input type='text' class='form-control input-sm leave_date' name='doctor_employment_histories[" + f + "][leave_date]' id='leave_date' placeholder='Leave Date'></div></div><div class='form-group'><div class='col-md-6'><label class='checkbox-inline'><input type='checkbox' name='doctor_employment_histories[" + f + "][continue]' value='1'>Continue</label></div></div><div class='form-group'><div class='col-md-12'><textarea name='doctor_employment_histories[" + f + "][job_responsibility]' class='form-control' id='job_responsibility' placeholder='Job Responsibility'></textarea></div></div></div></div></div></div>";

                }
            };

            var x = 1;
            var y = 1;

            $(addButton).click(function () {
                if (x < maxField) {
                    x++;
                    y++;
                    $(wrapper).append(fieldHTML.row(y));
                }
            });
            $(wrapper).on('click', '.remove_button', function (e) {
                e.preventDefault();
                $(this).closest('.exp').fadeOut(300, function () {
                    $(this).remove();
                });
                x--;
            });

        }

        addMoreExperience();



    