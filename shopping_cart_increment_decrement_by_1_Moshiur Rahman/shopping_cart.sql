drop database if exists shopping_cart;
create database shopping_cart;
use shopping_cart;
create table products(id int(10) auto_increment primary key, product_name text, price text, uom text);
insert into products(product_name,price,uom)values('mobile','5000','pcs');
insert into products(product_name,price,uom)values('headPhone','500','pcs');
insert into products(product_name,price,uom)values('laptop','50000','pcs');